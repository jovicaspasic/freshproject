<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController0 extends Controller
{
    public function show($post)
    {
//        return 'Hello';

        $posts = [
            'my-first-post' => 'Hello this is the first post.',
            'my-second-post' => 'This is the second post and we wil tra lalalla.'
        ];

        if (! array_key_exists($post, $posts)){
            abort(404, 'Sorry, that post was not found!');
        }

        return view('post',[
            'post' =>$posts[$post]
        ]);
    }
}
