<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show($slug)
    {

        $post = \DB::table('posts')->where('slug', $slug)->first();
        dd($post);
//        $posts = [
//            'my-first-post' => 'Hello this is the first post.',
//            'my-second-post' => 'This is the second post and we wil tra lalalla.'
//        ];

        if (! array_key_exists($post, $posts)){
            abort(404, 'Sorry, that post was not found!');
        }

        return view('post',[
            'post' =>$posts[$post]
        ]);
    }
}
