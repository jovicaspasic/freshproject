<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('/post/{post}', function ($post) {
//
//    //key = $post
//    //array = $posts
//
//    $posts = [
//        'my-first-post' => 'Hello this is the first post.',
//        'my-second-post' => 'This is the second post and we wil tra lalalla.'
//    ];
//
//    if (! array_key_exists($post, $posts)){
//        abort(404, 'Sorry, that post was not found!');
//    }
//
//    return view('post',[
//        'post' =>$posts[$post] ?? 'Nothing here yet!'
//        ]);
//});
Route::get('/post/{post}', 'PostsController@show');